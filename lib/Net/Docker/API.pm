package Net::Docker::API;
use strict;
use 5.20.0;
our $VERSION = '0.01';

use Moo;
use JSON;
use URI;
use URI::QueryParam;
use LWP::UserAgent;
use Carp;
use AnyEvent;
use AnyEvent::Socket 'tcp_connect';
use AnyEvent::HTTP;

has address => (
  is => 'ro',
  default => sub { $ENV{DOCKER_HOST} || 'http:/var/run/docker.sock/' }
);
has ua => (
  is => 'lazy'
);

sub _build_ua {
  my $self = shift;
  if ( $self->address !~ m!http://! ) {
    require LWP::Protocol::http::SocketUnixAlt;
    LWP::Protocol::implementor( http => 'LWP::Protocol::http::SocketUnixAlt' );
  }
  my $ua = LWP::UserAgent->new;
  return $ua;
}

sub _uri { my $self = shift; return $self->uri(@_); }

sub uri {
  my ( $self, $rel, %options ) = @_;
  my $uri = URI->new( $self->address . $rel );
  $uri->query_form(%options);
  return $uri;
}

sub _parse {
  my ( $self, $uri, %options ) = @_;
  my $res = $self->ua->get( $self->_uri( $uri, %options ) );
  if ( $res->content_type eq 'application/json' ) {
    return decode_json( $res->decoded_content );
  }
  elsif ( $res->content_type eq 'text/plain' ) {
    return eval { decode_json( $res->decoded_content ) };
  }
  $res->dump;
}

sub _parse_request {
  my ( $self, $res ) = @_;
  if ( $res->content_type eq 'application/json' ) {
    my $json = JSON::XS->new;
    return $json->incr_parse( $res->decoded_content );
  }
  my $message = $res->decoded_content;
  $message =~ s/\r?\n$//;
  croak $message;
}

# TODO: test it
sub create {
  my ( $self, %options ) = @_;
  $options{AttachStderr} //= \1;
  $options{AttachStdout} //= \1;
  $options{AttachStdin}  //= \0;
  $options{OpenStdin}    //= \0;
  $options{Tty}          //= \1;

  ## workaround for an odd API implementation of
  ## container naming
  my %query;
  if ( my $container_id = delete $options{Name} ) {
    $query{name} = $container_id;
  }

  my $input = encode_json( \%options );

  my $res =
    $self->ua->post( $self->uri( '/containers/create', %query ), 'Content-Type' => 'application/json', Content => $input );

  my $json = JSON::XS->new;
  my $out  = $json->incr_parse( $res->decoded_content );
  return $out->{Id};
}

sub images {
  my ( $self, %options ) = @_;
  return $self->_parse( '/images/json', %options );
}

# TODO: test it
sub images_viz {
  my ( $self, %options ) = @_;
  return $self->_parse( '/images/viz', %options );
}

# TODO: test it
sub search {
  my ( $self, %options ) = @_;
  return $self->_parse( '/images/search', %options );
}

# TODO: test it
sub history {
  my ( $self, $image_id, %options ) = @_;
  return $self->_parse( '/images/' . $image_id . '/history', %options );
}

# TODO: test it
sub inspect {
  my ( $self, $image_id, %options ) = @_;
  return $self->_parse( '/images/' . $image_id . '/json', %options );
}

sub version {
  my ( $self, %options ) = @_;
  return $self->_parse( '/version', %options );
}

# TODO: test it
sub info {
  my ( $self, %options ) = @_;
  return $self->_parse( '/info', %options );
}

sub containers {
  my ( $self, %options ) = @_;
  $options{all}=1;
  return $self->_parse( '/containers/json', %options );
}

sub container {
  my ( $self, $container_id, %options ) = @_;
  return $self->_parse( '/containers/' . $container_id . '/json', %options );
}

sub stats {
  my ($self, $container_id, %options ) = @_;
  $options{stream}=0;
  if (!$container_id) {
    my @stats;
    for my $container ( @{ $self->_parse('/containers/json', %options) } ) {
      push( @stats, $self->_parse('/containers/' . $container->{Id} . '/stats', %options) );
    }
    return [ @stats ];
  }
  return $self->_parse( '/containers/' . $container_id . '/stats', %options );
}

# TODO: test it
sub export {
  my ( $self, $container_id, %options ) = @_;
  return $self->_parse( '/containers/' . $container_id . '/export', %options );
}

# TODO: test it
sub diff {
  my ( $self, $container_id, %options ) = @_;
  return $self->_parse( '/containers/' . $container_id . '/changes', %options );
}

# TODO: test it
sub remove_image {
  my ( $self, @image_ids ) = @_;
  for my $image_id (@image_ids) {
    $self->ua->request( HTTP::Request->new( 'DELETE', $self->_uri( '/images/' . $image_id ) ) );
  }
  return;
}

# TODO: test it
sub remove_container {
  my ( $self, @container_ids ) = @_;
  for my $container (@container_ids) {
    $self->ua->request( HTTP::Request->new( 'DELETE', $self->_uri( '/containers/' . $container ) ) );
  }
  return;
}

# TODO: test it
sub pull {
  my ( $self, $repository, $tag, $registry ) = @_;

  if ( $repository =~ m/:/ ) {
    ( $repository, $tag ) = split /:/, $repository;
  }
  my %options = (
    fromImage => $repository,
    tag       => $tag,
    registry  => $registry,
  );
  my $uri = '/images/create';
  my $res = $self->ua->post( $self->_uri( $uri, %options ) );
  return $self->_parse_request($res);
}

# TODO: test it
sub start {
  my ( $self, $container_id, %options ) = @_;
  $self->ua->post( $self->_uri( '/containers/' . $container_id . '/start' ) );
  return;
}

# TODO: test it
sub stop {
  my ( $self, $container_id, %options ) = @_;
  $self->ua->post( $self->_uri( '/containers/' . $container_id . '/stop' ) );
  return;
}

1;

=head1 NAME

Net::Docker - Interface to the Docker API

=head1 SYNOPSIS

    use Net::Docker;

    my $api = Net::Docker::API->new;

    ...

=head1 DESCRIPTION

Perl module for using the Docker Remote API.

=head1 AUTHOR

Peter Stuifzand E<lt>net-docker-api@sysdef.deE<gt>

=head1 COPYRIGHT

Copyright 2019 - Juergen 'sysdef' Heine

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

L<http://docker.io>

=cut
